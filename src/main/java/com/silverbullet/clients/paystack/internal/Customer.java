/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.internal;

import com.google.gson.annotations.SerializedName;
import com.silverbullet.clients.paystack.response.PaystackResponse;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author prodigy4440
 */
public final class Customer implements Serializable {

    private static final long serialVersionUID = 7951349191930051929L;

    private String email;

    private Long integration;

    private String domain;

    @SerializedName("customer_code")
    private String customerCode;

    private Long id;

    @SerializedName("createdAt")
    private Date createdDate;

    @SerializedName("updateAt")
    private Date updatedDate;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("phone")
    private String phoneNumber;

    public Customer() {
    }

    public Customer(String email, Long integration, String domain,
            String customerCode, Long id, Date createdDate, Date updatedDate,
            String firstName, String lastName) {
        this.email = email;
        this.integration = integration;
        this.domain = domain;
        this.customerCode = customerCode;
        this.id = id;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Customer(PaystackResponse paystackResponse) {
        setCreatedDate(paystackResponse.getData().getCreatedDate());
        setCustomerCode(paystackResponse.getData().getCustomerCode());
        setDomain(paystackResponse.getData().getDomain());
        setEmail(paystackResponse.getData().getEmail());
        setFirstName(paystackResponse.getData().getFirstName());
        setId(paystackResponse.getData().getId());
        setIntegration(paystackResponse.getData().getIntegration());
        setLastName(paystackResponse.getData().getLastName());
        setUpdatedDate(paystackResponse.getData().getUpdatedDate());
        setPhoneNumber(paystackResponse.getData().getPhoneNumber());
    }

    public Customer(com.silverbullet.clients.paystack.response.Customer customer) {
        setCreatedDate(customer.getCreatedDate());
        setCustomerCode(customer.getCustomerCode());
        setDomain(customer.getDomain());
        setEmail(customer.getEmail());
        setFirstName(customer.getLastName());
        setId(customer.getId());
        setIntegration(customer.getIntegration());
        setLastName(customer.getLastName());
        setPhoneNumber(customer.getPhoneNumber());
        setUpdatedDate(customer.getUpdatedDate());
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIntegration() {
        return integration;
    }

    public void setIntegration(Long integration) {
        this.integration = integration;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Customer{" + "email=" + email + ", integration=" + integration + ", domain=" + domain + ", customerCode=" + customerCode + ", id=" + id + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + ", firstName=" + firstName + ", lastName=" + lastName + ", phoneNumber=" + phoneNumber + '}';
    }

}
