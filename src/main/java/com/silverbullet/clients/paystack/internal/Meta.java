/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.internal;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 *
 * @author prodigy4440
 */
public class Meta implements Serializable {

    private static final long serialVersionUID = 1100870501474969613L;

    private int total;
    @SerializedName("total_volume")
    private int totalVolume;
    private int skipped;
    private int perPage;
    private int page;
    private int pageCount;

    public Meta() {
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(int totalVolume) {
        this.totalVolume = totalVolume;
    }

    public int getSkipped() {
        return skipped;
    }

    public void setSkipped(int skipped) {
        this.skipped = skipped;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    @Override
    public String toString() {
        return "Meta{" + "total=" + total + ", totalVolume=" + totalVolume 
                + ", skipped=" + skipped + ", perPage=" + perPage + 
                ", page=" + page + ", pageCount=" + pageCount + '}';
    }
    
}
