/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.internal;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author prodigy4440
 */
public class TransactionAuth implements Serializable{

    private static final long serialVersionUID = -5178602698905627468L;
    
    private String url;
    private String accessCode;
    private String reference;

    public TransactionAuth() {
    }

    public TransactionAuth(String url, String accessCode, String reference) {
        this.url = url;
        this.accessCode = accessCode;
        this.reference = reference;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.url);
        hash = 59 * hash + Objects.hashCode(this.accessCode);
        hash = 59 * hash + Objects.hashCode(this.reference);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TransactionAuth other = (TransactionAuth) obj;
        if (!Objects.equals(this.url, other.url)) {
            return false;
        }
        if (!Objects.equals(this.accessCode, other.accessCode)) {
            return false;
        }
        if (!Objects.equals(this.reference, other.reference)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Authorization{" + "url=" + url + ", accessCode=" + accessCode + 
                ", reference=" + reference + '}';
    }
    
    
}
