/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.internal;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author prodigy4440
 * @param <T> Type of response to be sent out
 */
public class Response<T> {

    private Status status;
    private List<T> items;

    public Response() {
        this.status = new Status("No call initiated", Boolean.TRUE);
        this.items = null;
    }

    public Response(Status status, List<T> items) {
        this.status = status;
        this.items = items;
    }

    public Status getStatus() {
        return status;
    }

    public List<T> getItems() {
        return items;
    }

    public T getItem() {
        return items.get(0);
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.status);
        hash = 29 * hash + Objects.hashCode(this.items);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Response<?> other = (Response<?>) obj;
        if (!Objects.equals(this.status, other.status)) {
            return false;
        }
        if (!Objects.equals(this.items, other.items)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Response{" + "status=" + status + ", items=" + items + '}';
    }

}
