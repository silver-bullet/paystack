/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.response;

import java.io.Serializable;

/**
 *
 * @author prodigy4440
 */
public class History implements Serializable {

    private String type;
    private String message;
    private int time;

    public History() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "History{" + "type=" + type + ", message=" + message + ", time=" + time + '}';
    }
    
}
