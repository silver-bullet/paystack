/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.internal;

/**
 *
 * @author prodigy4440
 */
public class Status {

    private String description;
    private Boolean status;

    public Status() {
    }

    public Status(String description, Boolean status) {
        this.description = description;
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Status{" + "description=" + description + ", status=" + status + '}';
    }

}
