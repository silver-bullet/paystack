/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.silverbullet.clients.paystack.internal.Meta;
import com.silverbullet.clients.paystack.internal.Transaction;
import com.silverbullet.clients.paystack.util.Pair;
import com.silverbullet.clients.paystack.response.Customer;
import com.silverbullet.clients.paystack.response.PaystackResponse;
import com.silverbullet.clients.paystack.util.JsonUtil;
import java.io.IOException;
import java.util.Objects;
import okhttp3.HttpUrl;
import okhttp3.MediaType;

/**
 *
 * @author prodigy4440
 */
public class PaystackApi {

    public static PaystackResponse initTransaction(String secret, String email, 
            double amount) throws IOException {

        int chargeAmount = (int) (100 * amount);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("email", email);
        jsonObject.addProperty("amount", chargeAmount);

        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
        String body = JsonUtil.toJson(jsonObject);
        Pair<String, String> autorization
                = Pair.createPair("authorization", "Bearer " + secret);
        Pair<String, String> contentType = Pair.createPair("content-type", "application/json");
        Pair<String, String> cacheControl = Pair.createPair("cache-control", "no-cache");

        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host("api.paystack.co")
                .addPathSegment("transaction")
                .addPathSegment("initialize")
                .build();

        return JsonUtil.fromJson(RestClient.post(httpUrl, mediaType, body,
                autorization, contentType, cacheControl).body().string(), 
                PaystackResponse.class);
    }

    public static PaystackResponse initTransaction(String secret, String email, 
            double amount, String callbackUrl) throws IOException {

        int chargeAmount = (int) (100 * amount);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("email", email);
        jsonObject.addProperty("amount", chargeAmount);
        
        if(Objects.nonNull(callbackUrl)){
            jsonObject.addProperty("callback_url", callbackUrl);
        }

        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
        String body = JsonUtil.toJson(jsonObject);
        Pair<String, String> autorization
                = Pair.createPair("authorization", "Bearer " + secret);
        Pair<String, String> contentType = Pair.createPair("content-type", "application/json");
        Pair<String, String> cacheControl = Pair.createPair("cache-control", "no-cache");

        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host("api.paystack.co")
                .addPathSegment("transaction")
                .addPathSegment("initialize")
                .build();

        return JsonUtil.fromJson(RestClient.post(httpUrl, mediaType, body,
                autorization, contentType, cacheControl).body().string(), 
                PaystackResponse.class);
    }

    public static PaystackResponse verifyTransaction(String secret, String reference)
            throws IOException {
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host("api.paystack.co")
                .addPathSegment("transaction")
                .addPathSegment("verify")
                .addPathSegment(reference)
                .build();

        
        String response = RestClient.get(httpUrl,
                Pair.createPair("authorization", "Bearer " + secret),
                Pair.createPair("Content-Type", "application/json"))
                .body().string();
        
        return JsonUtil.fromJson(response, PaystackResponse.class);
    }

    public static PaystackResponse chargeAuthorization(String secret, 
            String authorizationCode, String email, double amount) 
            throws IOException {
        int chargeAmount = (int) (100 * amount);
        
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("authorization_code", authorizationCode);
        jsonObject.addProperty("email", email);
        jsonObject.addProperty("amount", chargeAmount);

        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host("api.paystack.co")
                .addPathSegment("transaction")
                .addPathSegment("charge_authorization")
                .build();

        return JsonUtil.fromJson(RestClient.post(httpUrl,
                MediaType.parse("application/json; charset=utf-8"),
                JsonUtil.toJson(jsonObject),
                Pair.createPair("authorization", "Bearer " + secret),
                Pair.createPair("Content-Type", "application/json")).body()
                .string(), PaystackResponse.class);

    }

    public static PaystackResponse createCustomer(String secret, 
            String firstName, String lastName, String email, String phoneNumber)
            throws IOException {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("first_name", firstName);
        jsonObject.addProperty("last_name", lastName);
        jsonObject.addProperty("email", email);
        jsonObject.addProperty("phone", phoneNumber);

        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host("api.paystack.co")
                .addPathSegment("customer")
                .build();

        return JsonUtil.fromJson(RestClient.post(httpUrl,
                MediaType.parse("application/json; charset=utf-8"),
                JsonUtil.toJson(jsonObject),
                Pair.createPair("authorization", "Bearer " + secret),
                Pair.createPair("Content-Type", "application/json")).body()
                .string(), PaystackResponse.class);
    }

    public static PaystackResponse listCustomers(String secret) 
            throws IOException {
        HttpUrl httpUrl = new HttpUrl.Builder().scheme("https")
                .host("api.paystack.co")
                .addPathSegment("customer")
                .build();

        String string = RestClient.get(httpUrl,
                Pair.createPair("authorization", "Bearer " + secret))
                .body().string();

        JsonParser jsonParser = new JsonParser();

        JsonObject jsonObject = jsonParser.parse(string).getAsJsonObject();
        JsonArray jsonArray = jsonObject.get("data").getAsJsonArray();

        PaystackResponse paystackResponse = new PaystackResponse();
        paystackResponse.setMessage(jsonObject.get("message").getAsString());
        paystackResponse.setStatus(jsonObject.get("status").getAsBoolean());
        paystackResponse.setCustomers(JsonUtil.fromJson(jsonArray.toString(),
                Customer[].class));

        return paystackResponse;
    }

    public static PaystackResponse fetchCustomer(String secret, 
            String customerIdOrCode) throws IOException {
        HttpUrl httpUrl = new HttpUrl.Builder().scheme("https")
                .host("api.paystack.co")
                .addPathSegment("customer")
                .addPathSegment(customerIdOrCode)
                .build();

        String string = RestClient.get(httpUrl,
                Pair.createPair("authorization", "Bearer " + secret))
                .body().string();

        JsonParser jsonParser = new JsonParser();

        JsonObject jsonObject = jsonParser.parse(string).getAsJsonObject();
        JsonObject cusJsonObject = jsonObject.get("data").getAsJsonObject();
        Customer customer = JsonUtil.fromJson(cusJsonObject.toString(), Customer.class);

        PaystackResponse paystackResponse = new PaystackResponse();
        paystackResponse.setMessage(jsonObject.get("message").getAsString());
        paystackResponse.setStatus(jsonObject.get("status").getAsBoolean());
        paystackResponse.setCustomer(customer);

        return paystackResponse;
    }

    public static PaystackResponse updateCustomer(String secret, 
            String customerIdOrCode, String firstName, String lastName, 
            String phoneNumber) throws IOException {
        JsonObject jsonObject = new JsonObject();
        if (Objects.isNull(firstName) || firstName.isEmpty()) {

        } else {
            jsonObject.addProperty("first_name", firstName);
        }

        if (Objects.isNull(lastName) || lastName.isEmpty()) {

        } else {
            jsonObject.addProperty("last_name", lastName);
        }

        if (Objects.isNull(phoneNumber) || phoneNumber.isEmpty()) {

        } else {
            jsonObject.addProperty("phone", phoneNumber);
        }

        HttpUrl build = new HttpUrl.Builder().scheme("https")
                .host("api.paystack.co")
                .addPathSegment("customer")
                .addPathSegment(customerIdOrCode)
                .build();
        String string = RestClient.post(build,
                MediaType.parse("application/json;charset=utf-8"),
                JsonUtil.toJson(jsonObject), Pair.createPair("authorization",
                "Bearer " + secret),
                Pair.createPair("Content-Type", "application/json")).body().string();

        PaystackResponse paystackResponse = new PaystackResponse();
        JsonParser jsonParser = new JsonParser();
        JsonObject responseJsonObject = jsonParser.parse(string).getAsJsonObject();
        boolean status = responseJsonObject.get("status").getAsBoolean();
        paystackResponse.setMessage(responseJsonObject.get("message").getAsString());
        paystackResponse.setStatus(status);

        if (status) {
            JsonObject cusJsonObject = responseJsonObject.get("data").getAsJsonObject();
            Customer customer = JsonUtil.fromJson(cusJsonObject.toString(), Customer.class);
            paystackResponse.setCustomer(customer);
        }

        return paystackResponse;
    }

    public static PaystackResponse whiteOrBlackListCustomer(String secret,
            String customerIdCodeOrEmail, String riskAction)
            throws IOException {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("customer", customerIdCodeOrEmail);
        //Risk Action can either be allow of deny
        jsonObject.addProperty("risk_action", riskAction);

        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host("api.paystack.co")
                .addPathSegment("customer")
                .addPathSegment("set_risk_action")
                .build();

        String string = RestClient.post(httpUrl,
                MediaType.parse("application/json;charset=utf-8"),
                JsonUtil.toJson(jsonObject),
                Pair.createPair("authorization", "Bearer " + secret),
                Pair.createPair("Content-Type", "application/json"))
                .body().string();

        PaystackResponse paystackResponse = new PaystackResponse();
        JsonParser jsonParser = new JsonParser();
        JsonObject responseJsonObject = jsonParser.parse(string).getAsJsonObject();
        boolean status = responseJsonObject.get("status").getAsBoolean();
        paystackResponse.setMessage(responseJsonObject.get("message").getAsString());
        paystackResponse.setStatus(status);

        if (status) {
            JsonObject cusJsonObject = responseJsonObject.get("data").getAsJsonObject();
            Customer customer = JsonUtil.fromJson(cusJsonObject.toString(), Customer.class);
            paystackResponse.setCustomer(customer);
        }

        return paystackResponse;
    }

    public static PaystackResponse deactivateAuthorization(String secret, 
            String authorizationCode) throws IOException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("authorization_code", authorizationCode);

        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host("api.paystack.co")
                .addPathSegment("customer")
                .addPathSegment("deactivate_authorization")
                .build();

        String string = RestClient.post(httpUrl,
                MediaType.parse("application/json;charset=utf-8"),
                JsonUtil.toJson(jsonObject),
                Pair.createPair("authorization", "Bearer " + secret),
                Pair.createPair("Content-Type", "application/json"))
                .body().string();

        PaystackResponse paystackResponse = new PaystackResponse();
        JsonParser jsonParser = new JsonParser();
        JsonObject responseJsonObject = jsonParser.parse(string).getAsJsonObject();
        boolean status = responseJsonObject.get("status").getAsBoolean();
        paystackResponse.setMessage(responseJsonObject.get("message").getAsString());
        paystackResponse.setStatus(status);

        return paystackResponse;
    }

    public static PaystackResponse listTransactions(String secret, 
            Integer perPage, Integer page, Integer customer, String status, 
            String from, String to, Integer amount) 
            throws IOException {

        HttpUrl.Builder builder = new HttpUrl.Builder()
                .scheme("https")
                .host("api.paystack.co")
                .addPathSegment("transaction");
        if (Objects.nonNull(perPage)) {
            builder.addQueryParameter("perPage", Integer.toString(perPage));
        }
        if (Objects.nonNull(page)) {
            builder.addQueryParameter("page", Integer.toString(page));
        }
        if (Objects.nonNull(customer)) {
            builder.addQueryParameter("customer", Integer.toString(customer));
        }
        if (Objects.nonNull(status)) {
            builder.addQueryParameter("status", status);
        }
        if (Objects.nonNull(from)) {
            builder.addQueryParameter("from", from);
        }
        if (Objects.nonNull(to)) {
            builder.addQueryParameter("to", to);
        }
        if (Objects.nonNull(amount)) {
            builder.addQueryParameter("amount", Integer.toString(amount));
        }

        String string = RestClient.get(builder.build(),
                Pair.createPair("authorization", "Bearer " + secret)
        ).body().string();

        PaystackResponse paystackResponse = new PaystackResponse();
        JsonParser jsonParser = new JsonParser();
        JsonObject responseJsonObject = jsonParser.parse(string).getAsJsonObject();
        boolean responseStatus = responseJsonObject.get("status").getAsBoolean();
        paystackResponse.setMessage(responseJsonObject.get("message").getAsString());
        paystackResponse.setStatus(responseStatus);

        if (responseStatus) {
            JsonObject metaJsonObject = responseJsonObject.get("meta").getAsJsonObject();
            Meta meta = JsonUtil.fromJson(metaJsonObject.toString(), Meta.class);
            paystackResponse.setMeta(meta);

            JsonArray jsonArray = responseJsonObject.get("data").getAsJsonArray();
            Transaction[] transactions = JsonUtil.fromJson(jsonArray.toString(),
                    Transaction[].class);
            paystackResponse.setTransactions(transactions);
        }

        return paystackResponse;
    }

    public static PaystackResponse fetchTransaction(String secret, int id)
            throws IOException {
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .host("api.paystack.co")
                .addPathSegment("transaction")
                .addPathSegment(Integer.toString(id))
                .build();

        String string = RestClient.get(httpUrl,
                Pair.createPair("authorization", "Bearer " + secret))
                .body().string();

        PaystackResponse paystackResponse = new PaystackResponse();
        JsonParser jsonParser = new JsonParser();
        JsonObject responseJsonObject = jsonParser.parse(string).getAsJsonObject();
        boolean responseStatus = responseJsonObject.get("status").getAsBoolean();
        paystackResponse.setMessage(responseJsonObject.get("message").getAsString());
        paystackResponse.setStatus(responseStatus);

        if (responseStatus) {
            JsonObject trJsonObject = responseJsonObject.get("data").getAsJsonObject();
            Transaction transaction = JsonUtil.fromJson(trJsonObject.toString(),
                    Transaction.class);
            paystackResponse.setTransaction(transaction);
        }

        return paystackResponse;
    }

}
