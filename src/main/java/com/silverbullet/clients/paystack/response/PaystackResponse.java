/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.response;

import com.silverbullet.clients.paystack.internal.Meta;
import com.silverbullet.clients.paystack.internal.Transaction;
import java.io.Serializable;

/**
 *
 * @author prodigy4440
 */
public class PaystackResponse implements Serializable{

    private static final long serialVersionUID = 3482333847768429629L;
    private Boolean status;
    private String message;
    private Data data;
    private Customer [] customers;
    private Customer customer;
    private Meta meta;
    private Transaction transaction;
    private Transaction [] transactions;
    
    
    public PaystackResponse() {
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Customer[] getCustomers() {
        return customers;
    }

    public void setCustomers(Customer[] customers) {
        this.customers = customers;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Transaction[] getTransactions() {
        return transactions;
    }

    public void setTransactions(Transaction[] transactions) {
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return "PaystackResponse{" + "status=" + status + ", message=" + message + ", data=" + data + ", customers=" + customers + ", customer=" + customer + ", meta=" + meta + ", transaction=" + transaction + ", transactions=" + transactions + '}';
    }

    
}
