/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.response;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author prodigy4440
 */
public class Data implements Serializable {

    private static final long serialVersionUID = -8355197533598864756L;

    @SerializedName("authorization_url")
    private String authorizationUrl;

    @SerializedName("access_code")
    private String accessCode;

    private String reference;

    private Integer amount;

    private String currency;

    @SerializedName("transaction_date")
    private Date transactionDate;

    private String status;

    private String domain;

    private String email;
    private Long integration;

    @SerializedName("customer_code")
    private String customerCode;

    private Long id;

    @SerializedName("createdAt")
    private Date createdDate;
    
    @SerializedName("updatedAt")
    private Date updatedDate;

    @SerializedName("phone")
    private String phoneNumber;

    @SerializedName("gateway_response")
    private String gatewayResponse;

    private String message;

    private String channel;

    @SerializedName("ip_address")
    private String ipAddress;

    @SerializedName("first_name")
    private String firstName;
    
    @SerializedName("last_name")
    private String lastName;
    
    private Log log;

    private Authorization authorization;

    private Customer customer;

    private String plan;

    public Data() {
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAuthorizationUrl() {
        return authorizationUrl;
    }

    public void setAuthorizationUrl(String authorizationUrl) {
        this.authorizationUrl = authorizationUrl;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIntegration() {
        return integration;
    }

    public void setIntegration(Long integration) {
        this.integration = integration;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getGatewayResponse() {
        return gatewayResponse;
    }

    public void setGatewayResponse(String gatewayResponse) {
        this.gatewayResponse = gatewayResponse;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }

    public Authorization getAuthorization() {
        return authorization;
    }

    public void setAuthorization(Authorization authorization) {
        this.authorization = authorization;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    @Override
    public String toString() {
        return "Data{" + "authorizationUrl=" + authorizationUrl + ", accessCode=" + accessCode + ", reference=" + reference + ", amount=" + amount + ", currency=" + currency + ", transactionDate=" + transactionDate + ", status=" + status + ", domain=" + domain + ", email=" + email + ", integration=" + integration + ", customerCode=" + customerCode + ", id=" + id + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + ", gatewayResponse=" + gatewayResponse + ", message=" + message + ", channel=" + channel + ", ipAddress=" + ipAddress + ", firstName=" + firstName + ", lastName=" + lastName + ", log=" + log + ", authorization=" + authorization + ", customer=" + customer + ", plan=" + plan + '}';
    }

    
}
