/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.response;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author prodigy4440
 */
public class Log implements Serializable {

    private static final long serialVersionUID = 4639122336411433612L;

    @SerializedName("time_spent")
    private int timeSpent;
    private int attempts;
    private String authentication;
    private int errors;
    private boolean success;
    private boolean mobile;
//         "input":[ ],
    private String channel;
    
    private List<History> history;

    public Log() {
    }

    public int getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(int timeSpent) {
        this.timeSpent = timeSpent;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public int getErrors() {
        return errors;
    }

    public void setErrors(int errors) {
        this.errors = errors;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isMobile() {
        return mobile;
    }

    public void setMobile(boolean mobile) {
        this.mobile = mobile;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

    @Override
    public String toString() {
        return "Log{" + "timeSpent=" + timeSpent + ", attempts=" + attempts + ", authentication=" + authentication + ", errors=" + errors + ", success=" + success + ", mobile=" + mobile + ", channel=" + channel + ", history=" + history + '}';
    }
  
}
