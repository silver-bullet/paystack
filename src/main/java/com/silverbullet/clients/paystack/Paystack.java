/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack;

import com.silverbullet.clients.paystack.internal.Response;
import com.silverbullet.clients.paystack.internal.Status;
import com.silverbullet.clients.paystack.internal.TransactionAuth;
import com.silverbullet.clients.paystack.response.Authorization;
import com.silverbullet.clients.paystack.response.PaystackResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author prodigy4440
 */
public class Paystack {

    private static String SECRET;

    public static void setSecret(String secret) {
        Paystack.SECRET = secret;
    }

    public static class Transaction {

        public static Response begin(String email, double amount) {

            try {
                PaystackResponse paystackResponse = PaystackApi.initTransaction(SECRET, 
                        email, amount);

                if (paystackResponse.getStatus()) {
                    TransactionAuth ta = new TransactionAuth(paystackResponse
                            .getData().getAuthorizationUrl(), paystackResponse
                                    .getData().getAccessCode(), paystackResponse
                                    .getData().getReference());
                    return new Response<>(new Status(paystackResponse.getMessage(),
                            Boolean.TRUE), Arrays.asList(ta));
                } else {
                    return new Response<>(new Status(paystackResponse.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }

        public static Response begin(String email, double amount, String callbackUrl)
        {

            try {
                PaystackResponse paystackResponse = PaystackApi.initTransaction(SECRET, 
                        email, amount, callbackUrl);

                if (paystackResponse.getStatus()) {
                    TransactionAuth ta = new TransactionAuth(paystackResponse
                            .getData().getAuthorizationUrl(), paystackResponse
                                    .getData().getAccessCode(), paystackResponse
                                    .getData().getReference());
                    return new Response<>(new Status(paystackResponse.getMessage(),
                            Boolean.TRUE), Arrays.asList(ta));
                } else {
                    return new Response<>(new Status(paystackResponse.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }

        public static Response verify(String reference) {

            try {
                PaystackResponse pr = PaystackApi.verifyTransaction(SECRET, reference);

                if (pr.getStatus()) {
                    com.silverbullet.clients.paystack.internal.Transaction transaction
                            = new com.silverbullet.clients.paystack.internal.Transaction(pr);

                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.TRUE), Arrays.asList(transaction));
                } else {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }

        public static Response charge(String authorizationCode, String email,
                double amount) {

            try {
                PaystackResponse pr = PaystackApi.chargeAuthorization(SECRET,
                        authorizationCode, email, amount);

                if (pr.getStatus()) {
                    com.silverbullet.clients.paystack.internal.Transaction transaction
                            = new com.silverbullet.clients.paystack.internal.Transaction(pr);

                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.TRUE), Arrays.asList(transaction));
                } else {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }

        public static Response list(Integer perPage, Integer page, Integer customer,
                String status, String from, String to, Integer amount) {

            try {
                PaystackResponse pr = PaystackApi.listTransactions(SECRET,
                        perPage, page, customer, status, from, to, amount);

                if (pr.getStatus()) {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.TRUE), Arrays.asList(pr.getTransactions(), pr.getMeta()));
                } else {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }

        public static Response fetch(int id) {
            try {
                PaystackResponse pr = PaystackApi.fetchTransaction(SECRET, id);

                if (pr.getStatus()) {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.TRUE), Arrays.asList(pr.getTransaction()));
                } else {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }

    }

    public static class Customer {

        public static Response create(String firstName, String lastName,
                String email, String phoneNumber) {

            try {
                PaystackResponse pr = PaystackApi.createCustomer(SECRET, firstName,
                        lastName, email, phoneNumber);

                if (pr.getStatus()) {
                    com.silverbullet.clients.paystack.internal.Customer customer
                            = new com.silverbullet.clients.paystack.internal.Customer(pr);

                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.TRUE), Arrays.asList(customer));
                } else {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }

        public static Response list() {

            try {
                PaystackResponse pr = PaystackApi.listCustomers(SECRET);

                if (pr.getStatus()) {

                    List<com.silverbullet.clients.paystack.internal.Customer> customers = new LinkedList<>();
                    for (com.silverbullet.clients.paystack.response.Customer customer : pr.getCustomers()) {
                        customers.add(new com.silverbullet.clients.paystack.internal.Customer(customer));
                    }

                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.TRUE), customers);
                } else {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }

        public static Response fetch(String customerIdOrCode) {

            try {
                PaystackResponse pr = PaystackApi.fetchCustomer(SECRET, customerIdOrCode);

                if (pr.getStatus()) {
                    com.silverbullet.clients.paystack.internal.Customer customer
                            = new com.silverbullet.clients.paystack.internal.Customer(pr.getCustomer());

                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.TRUE), Arrays.asList(customer));
                } else {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }

        public static Response update(String customerIdOrCode, String firstName,
                String lastName, String phoneNumber) {
            try {
                PaystackResponse pr = PaystackApi.updateCustomer(SECRET, customerIdOrCode,
                        firstName, lastName, phoneNumber);

                if (pr.getStatus()) {
                    com.silverbullet.clients.paystack.internal.Customer customer
                            = new com.silverbullet.clients.paystack.internal.Customer(pr.getCustomer());

                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.TRUE), Arrays.asList(customer));
                } else {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }

        public static Response whiteOrBlackList(String customerIdOrCode,
                String riskAction) {
            try {
                PaystackResponse pr = PaystackApi.whiteOrBlackListCustomer(SECRET,
                        customerIdOrCode, riskAction);

                if (pr.getStatus()) {
                    com.silverbullet.clients.paystack.internal.Customer customer
                            = new com.silverbullet.clients.paystack.internal.Customer(pr.getCustomer());

                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.TRUE), Arrays.asList(customer));
                } else {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }

        public static Response deactAuth(String authorizationCode) {
            try {
                PaystackResponse pr = PaystackApi.deactivateAuthorization(SECRET, authorizationCode);

                if (pr.getStatus()) {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.TRUE), null);
                } else {
                    return new Response<>(new Status(pr.getMessage(),
                            Boolean.FALSE), null);
                }

            } catch (IOException ex) {
                return new Response(new Status(ex.getMessage(), Boolean.FALSE), null);
            }
        }
    }

    public static class Util {

        public static Response userAuth(String reference) {
            Response response = Transaction.verify(reference);
            if (response.getStatus().getStatus()) {
                com.silverbullet.clients.paystack.internal.Transaction transaction
                        = (com.silverbullet.clients.paystack.internal.Transaction) response.getItem();
                Authorization authorization = transaction.getAuthorization();

                return new Response<>(new Status("Authorization fetched",
                        Boolean.FALSE), Arrays.asList(authorization));
            } else {
                return response;
            }
        }
    }

}
