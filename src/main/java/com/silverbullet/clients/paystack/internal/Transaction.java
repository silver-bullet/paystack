/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.internal;

import com.google.gson.annotations.SerializedName;
import com.silverbullet.clients.paystack.response.Authorization;
import com.silverbullet.clients.paystack.response.Log;
import com.silverbullet.clients.paystack.response.PaystackResponse;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author prodigy4440
 */
public final class Transaction implements Serializable {

    private static final long serialVersionUID = -1093729905205225073L;

    private Long id;
    private Integer amount;
    private String currency;
    private Date transactionDate;
    private String status;
    private String reference;
    private String domain;
    @SerializedName("gateway_response")
    private String gatewayResponse;
    private String message;
    private String channel;
    private String ipAddress;
    private Log log;
    private String fees;
    private Authorization authorization;
    private Customer customer;

    public Transaction() {
    }

    public Transaction(Integer amount, String currency, Date transactionDate, 
            String status, String reference, String domain, String gatewayResponse, 
            String message, String channel, String ipAddress, Log log, String fees,
            Authorization authorization, Customer customer) {
        this.amount = amount;
        this.currency = currency;
        this.transactionDate = transactionDate;
        this.status = status;
        this.reference = reference;
        this.domain = domain;
        this.gatewayResponse = gatewayResponse;
        this.message = message;
        this.channel = channel;
        this.ipAddress = ipAddress;
        this.log = log;
        this.fees = fees;
        this.authorization = authorization;
        this.customer = customer;
    }

    public Transaction(PaystackResponse paystackResponse){
        setAmount(paystackResponse.getData().getAmount());
        setAuthorization(paystackResponse.getData().getAuthorization());
        setChannel(paystackResponse.getData().getChannel());
        setCurrency(paystackResponse.getData().getCurrency());
        setCustomer(new Customer(paystackResponse.getData().getCustomer()));
        setDomain(paystackResponse.getData().getDomain());
        setFees("");
        setGatewayResponse(paystackResponse.getData().getGatewayResponse());
        setIpAddress(paystackResponse.getData().getIpAddress());
        setLog(paystackResponse.getData().getLog());
        setMessage(paystackResponse.getData().getMessage());
        setReference(paystackResponse.getData().getReference());
        setStatus(paystackResponse.getData().getStatus());
        setTransactionDate(paystackResponse.getData().getTransactionDate());
       
    }
  
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getGatewayResponse() {
        return gatewayResponse;
    }

    public void setGatewayResponse(String gatewayResponse) {
        this.gatewayResponse = gatewayResponse;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public Authorization getAuthorization() {
        return authorization;
    }

    public void setAuthorization(Authorization authorization) {
        this.authorization = authorization;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Transaction{" + "id=" + id + ", amount=" + amount + ", currency=" + currency + ", transactionDate=" + transactionDate + ", status=" + status + ", reference=" + reference + ", domain=" + domain + ", gatewayResponse=" + gatewayResponse + ", message=" + message + ", channel=" + channel + ", ipAddress=" + ipAddress + ", log=" + log + ", fees=" + fees + ", authorization=" + authorization + ", customer=" + customer + '}';
    }

    
}
