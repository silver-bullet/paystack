/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.response;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 *
 * @author prodigy4440
 */
public class Authorization implements Serializable {

    private static final long serialVersionUID = 925596034883705465L;

    @SerializedName("authorization_code")
    private String authorizationCode;
    @SerializedName("card_type")
    private String cardType;
    private Long last4;
    @SerializedName("exp_month")
    private int expMonth;
    @SerializedName("exp_year")
    private int expYear;
    private int bin;
    private String bank;
    private String channel;
    private boolean reusable;
    @SerializedName("country_code")
    private String countryCode;

    public Authorization() {
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    public int getBin() {
        return bin;
    }

    public void setBin(int bin) {
        this.bin = bin;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public boolean isReusable() {
        return reusable;
    }

    public void setReusable(boolean reusable) {
        this.reusable = reusable;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public Long getLast4() {
        return last4;
    }

    public void setLast4(Long last4) {
        this.last4 = last4;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

    @Override
    public String toString() {
        return "Authorization{" + "authorizationCode=" + authorizationCode + 
                ", card_type=" + cardType + ", last4=" + last4 + ", expMonth=" + 
                expMonth + ", expYear=" + expYear + ", bin=" + bin + ", bank=" + 
                bank + ", channel=" + channel + ", reusable=" + reusable + 
                ", countryCode=" + countryCode + '}';
    }

}
