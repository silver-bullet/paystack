/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack.util;

import java.util.Random;

/**
 *
 * @author prodigy4440
 */
public class TokenGenerator {

    private static final String PASSWORD_STRING
            = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private static final String NUMBERS = "0123456789";

    private static final Random RANDOM = new Random();

    public static String generateToken(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(PASSWORD_STRING.charAt(RANDOM.nextInt(PASSWORD_STRING.length())));
        }
        return sb.toString();
    }

    public static String generatePin(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(NUMBERS.charAt(RANDOM.nextInt(NUMBERS.length())));
        }
        return sb.toString();
    }

}
