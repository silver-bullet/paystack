/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.clients.paystack;

import com.silverbullet.clients.paystack.util.Pair;
import java.io.IOException;
import java.util.Objects;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 *
 * @author prodigy4440
 */
public class RestClient {

    private static OkHttpClient CLIENT;

    private RestClient() {
        CLIENT = new OkHttpClient();
    }

    public static OkHttpClient getConnection() {
        if (Objects.isNull(CLIENT)) {
            CLIENT = new OkHttpClient();
        }

        return CLIENT;
    }

    public static Request createGetRequest(String url) {
        return new Request.Builder().url(url).build();
    }

    public static Response get(String url) throws IOException {
        return getConnection().newCall(createGetRequest(url)).execute();
    }

    public static Request createGetRequest(String url, Pair<String, String>... headers) {
        Request.Builder requestBuilder = new Request.Builder();
        for (Pair<String, String> pair : headers) {
            requestBuilder.addHeader(pair.getKey(), pair.getValue());
        }

        return requestBuilder.url(url).build();
    }

    public static Request createGetRequest(HttpUrl url, Pair<String, String>... headers) {
        Request.Builder requestBuilder = new Request.Builder();
        for (Pair<String, String> pair : headers) {
            requestBuilder.addHeader(pair.getKey(), pair.getValue());
        }

        return requestBuilder.url(url).build();
    }

    public static Request createPost(String url, MediaType mediaType, String body,
            Pair<String, String>... headers) {

        Request.Builder builder = new Request.Builder().url(url)
                .post(RequestBody.create(mediaType, body));

        for (Pair<String, String> pair : headers) {
            builder.addHeader(pair.getKey(), pair.getValue());
        }

        return builder.build();
    }

    public static Request createPost(HttpUrl url, MediaType mediaType, String body,
            Pair<String, String>... headers) {

        Request.Builder builder = new Request.Builder().url(url)
                .post(RequestBody.create(mediaType, body));

        for (Pair<String, String> pair : headers) {
            builder.addHeader(pair.getKey(), pair.getValue());
        }

        return builder.build();
    }
    
    public static Response get(String url, Pair<String, String>... headers)
            throws IOException {
        return getConnection().newCall(createGetRequest(url, headers)).execute();
    }
    
    public static Response get(HttpUrl url, Pair<String, String>... headers)
            throws IOException {
        return getConnection().newCall(createGetRequest(url, headers)).execute();
    }

    public static Response post(String url, MediaType mediaType, String body,
            Pair<String, String> ... headers) throws IOException {
        return getConnection().newCall(createPost(url, mediaType, body, headers))
                .execute();
    }

    public static Response post(HttpUrl url, MediaType mediaType, String body,
            Pair<String, String> ... headers) throws IOException {
        return getConnection().newCall(createPost(url, mediaType, body, headers))
                .execute();
    }

}
